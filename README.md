官方站点：[https://www.hemaphp.com](https://www.hemaphp.com)

使用教程：[https://www.kancloud.cn/he_ma/single](https://www.kancloud.cn/he_ma/single)

## 功能概述
```
1.支持多平台：微信小程序，支付宝小程序，和H5平台，页面可以后台DIY管理。
2.小程序页面支持后台DIY，后台修改后即可同步到发布的小程序页面。
3.点单模式：支持堂食，外卖，打包自取。
4.下单：支持多人同时，中途加菜，退单等。
5.订单提醒：支持小程序订阅消息，公众号模板消息，小票打印。
6.结算方式：微信，支付宝，余额，餐后线下等。
7.商品管理：单，多规格，起售数量，打包费，库存销量推荐等操作。
8.门店管理：餐桌，店员，口味，云设备，二维码，wifi，预约订桌等管理。
9.会员卡：付费开会员，积分等级，等级折扣，升级赠送，卡面定制等。
10.积分签到：签到赠送积分或余额，添加签到任务等。
11.会员充值：前后台充值操作，充值套餐，充值送余额，充值送优惠券等。
12.优惠券：现金券（满减），折扣券（满折），赠送券（满赠）等
13.财务统计：实时统计，七日趋势，日统计，月统计，指定时间段统计，报表下载，打印等。
14.小票打印：易联云，飞鹅，大趋等品牌打印机。（其它可扩展）
15.第三方配送：河马跑腿，闪送，UU跑腿，顺丰同城，达达配送，码科配送（其它可扩展）
16.小票打印：易联云，飞鹅，大趋。（其它可扩展）
17.云存储：腾云，阿里，七牛（其它可扩展）
18.短信：腾云，阿里，七牛（其它可扩展）
```
**更多功能请看下方，进行项目体验**

## 项目演示
演示站点：[https://single.hemaphp.com](https://single.hemaphp.com)

**体验账号密码均为：test**

## 前端演示 - 聚合二维码
 **支持：微信小程序、支付宝小程序、H5。（由于未发布微信端，目前仅支持支付宝扫码）** 
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%B0%8F%E7%A8%8B%E5%BA%8F/qrcode-shop.png)
```
聚合码扫码说明
1.微信扫码：如果打开的不是微信小程序而是H5页面，说明微信小程序没有发布上线,
1.支付宝扫码：如果打开的不是支付宝小程序而是H5页面，说明支付宝小程序没有发布上线,
```
## 商业版咨询(微信客服)

![输入图片说明](https://www.hemaphp.com/assets/img/wechat.png)

 **购买商业版本请通过上述联系方式，其它都是骗子** 
## 小程序略图
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%B0%8F%E7%A8%8B%E5%BA%8F/1.png)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%B0%8F%E7%A8%8B%E5%BA%8F/2.png)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%B0%8F%E7%A8%8B%E5%BA%8F/3.png)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%B0%8F%E7%A8%8B%E5%BA%8F/4.png)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%B0%8F%E7%A8%8B%E5%BA%8F/5.png)
## 项目说明
本项目为免费开源项目，用于二开和学习。希望我们提供的代码对于你们的二开节省时间，对于你们的学习提供帮助。

技术问题可随时在线咨询，我们提供技术帮助。“不懂的”请绕行。