<?php
namespace app\api\model;

use app\common\model\Page as PageModel;

class Page extends PageModel
{
    /**
     * 隐藏字段
     * @var array
     */
    protected $hidden = [];
}
