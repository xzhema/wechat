<?php
namespace app\api\model;

use app\common\model\Address as AddressModel;

/**
 * 收货地址模型
 */
class Address extends AddressModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [];
}
