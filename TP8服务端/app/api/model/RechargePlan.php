<?php
namespace app\api\model;

use app\common\model\RechargePlan as RechargePlanModel;

/**
 * 充值套餐模型
 */
class RechargePlan extends RechargePlanModel
{
    /**
     * 隐藏字段
     * @var array
     */
    protected $hidden = [];
}
