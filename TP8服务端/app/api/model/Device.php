<?php
namespace app\api\model;

use app\common\model\Device as DeviceModel;

class Device extends DeviceModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [];
}
