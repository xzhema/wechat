<?php
namespace app\api\model;

use app\common\model\Setting as SettingModel;

class Setting extends SettingModel
{
    /**
     * 隐藏字段
     * @var array
     */
    protected $hidden = [];
}
