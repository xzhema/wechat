<?php
namespace app\api\model;

use app\common\model\Category as CategoryModel;

class Category extends CategoryModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [];

}
