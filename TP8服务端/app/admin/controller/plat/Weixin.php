<?php
namespace app\admin\controller\plat;

use app\admin\controller\Controller;
use app\admin\model\Setting as SettingModel;
use think\facade\View;

/**
 * 微信平台控制器
 */
class Weixin extends Controller
{
    /**
	 * 微信小程序订阅消息设置
	 */
	public function wxapptpl()
	{
	    return $this->updateEvent('wxapptpl');
	}
	
	/**
	 * 微信小程序设置
	 */
	public function wxapp()
	{
	    return $this->updateEvent('wxapp');
	}
	
	/**
	 * 公众号设置
	 */
	public function wechat()
	{
	    return $this->updateEvent('wechat');
	}

    /**
     * 更新设置事件
     */
    private function updateEvent(string $key)
    {
        if (!$this->request->isAjax()) {
            $model = SettingModel::getItem($key);
            return View::fetch($key, compact('model'));
        }
        $model = new SettingModel;
        if ($model->edit($key,$this->postData('data'))) {
            return $this->renderSuccess('更新成功');
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
}
