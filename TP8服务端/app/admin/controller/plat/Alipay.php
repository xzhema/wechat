<?php
namespace app\admin\controller\plat;

use app\admin\controller\Controller;
use app\admin\model\Setting as SettingModel;
use think\facade\View;

/**
 * 支付宝平台控制器
 */
class Alipay extends Controller
{
    /**
	 * 支付宝小程序设置
	 */
	public function aliapp()
	{
	    return $this->updateEvent('aliapp');
	}
	/**
	 * 支付宝网页应用设置
	 */
	public function aliweb()
	{
	    return $this->updateEvent('aliweb');
	}

    /**
     * 更新设置事件
     */
    private function updateEvent(string $key)
    {
        if (!$this->request->isAjax()) {
            $model = SettingModel::getItem($key);
            return View::fetch($key, compact('model'));
        }
        $model = new SettingModel;
        if ($model->edit($key,$this->postData('data'))) {
            return $this->renderSuccess('更新成功');
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
}
