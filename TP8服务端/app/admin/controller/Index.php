<?php
namespace app\admin\controller;

use app\admin\model\Comment as CommentModel;
use app\admin\model\User as UserModel;
use app\admin\model\Order as OrderModel;
use app\admin\model\Goods as GoodsModel;
use app\admin\model\Pact as PactModel;
use app\admin\model\RechargeLog as RechargeLogModel;
use think\facade\View;

/**
 * 后台首页
 */
class Index extends Controller
{
    
    public function index()
    {
    	$count = [
    	    'score' => CommentModel::score(),
    	    'user' => UserModel::getCount(),
    	    'order' => OrderModel::getCount(),
    	    'goods' => GoodsModel::getCount(),
    	    'pact' => PactModel::getCount(),
    	    'recharge' => RechargeLogModel::getCount(),
    	];
    	return View::fetch('index', compact('count'));
    }
}
