<?php
namespace app\admin\controller\order;

use app\admin\controller\Controller;
use app\admin\model\Pact as PactModel;
use think\facade\View;

/**
 * 预约控制器
 */
class Pact extends Controller
{
	
    /**
     * 预约管理
     */
    public function index($search='')
    {
        $model = new PactModel;
        $list = $model->getList(0,$search);
        return View::fetch('index', compact('list','search'));
    }

	/**
     * 状态编辑
     */
    public function status($id)
    {
        $model = PactModel::get($id);
		if ($model->status(30)) {
            return $this->renderSuccess('更新成功');
        }
        $error = $model->getError() ?: '更新失败';
    }
	
}
